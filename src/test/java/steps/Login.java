package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {
	public ChromeDriver driver;
	@Given("Launch the browser")
	public void launchBrowser() {
		driver = new ChromeDriver();
	}    
	@And("Load the URL")
	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps");
	}
	@When("Maximize the browser")
	public void maximize() {
		driver.manage().window().maximize();
	}
	@And("Set timeout")
	public void timeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@When("Enter the username as (.*)")
	public void typeUsernameAsDemoSLM(String data) {
		driver.findElementById("username").sendKeys(data);
	}
	@And("Enter the password as (.*)")
	public void typePassword(String data) {
		driver.findElementById("password").sendKeys(data);
	}
	@When("Click on login button")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Then("Verify login is success")
	public void verify() {
//		System.out.println("Login verified");
		String text = driver.findElementByXPath("//h2").getText();
		if(text.contains("Demo Sales Manager")) {
			System.out.println("Passed");
		}else {
			throw new RuntimeException();
		}
	}
	@Then("Close browser")
	public void closeBrowser() {
		driver.close();
	}
}